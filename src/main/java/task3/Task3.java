package task3;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task3 {
    private Random rand = new Random(47);
    public List<Integer> iterateList(int quantity) {
        List<Integer> result = Stream
                .iterate(rand.nextInt(5000), n -> rand.nextInt(5000))
                .limit(quantity)
                .collect(Collectors.toList());
        return result;
    }


    public List<Integer> generateList(int quantity) {
        List<Integer> result = Stream
                .generate(()->rand.nextInt(5000))
                .limit(quantity)
                .collect(Collectors.toList());
        return result;
    }

    public List<Integer> streamOf() {
        return Stream
                .of(rand.nextInt(5000), rand.nextInt(5000),
                        rand.nextInt(5000), rand.nextInt(5000))
                .collect(Collectors.toList());
    }

    public List<Integer> streamBuilder() {
        return Stream.<Integer>builder().add(rand.nextInt(5000))
                .add(rand.nextInt(5000))
                .add(rand.nextInt(5000))
                .add(rand.nextInt(5000))
                .build()
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        Task3 t = new Task3();
        List<Integer> iterated = t.iterateList(15);
        System.out.println(iterated);
        Stream<Integer> iteratedStream = iterated.stream();
        System.out.println(iteratedStream.max((i1, i2) -> i1 - i2));
        System.out.println();

        List<Integer> generated = t.generateList(15);
        System.out.println(generated);
        Stream<Integer> generatedStream = generated.stream();
        int average = generatedStream.reduce(Integer::sum).get() / 15;
        System.out.println("Average: " + average);
        Stream<Integer> countBiggerThanAverage = generated.stream();
        long count = countBiggerThanAverage.filter(n -> n > average).count();
        System.out.println("> average: " + count);
        System.out.println();

        List<Integer> streamOf = t.streamOf();
        System.out.println(streamOf);
        Stream<Integer> streamOfStream = streamOf.stream();
        System.out.println(streamOfStream.reduce(0, (a, b) -> a + b));
        System.out.println();

    }
}
