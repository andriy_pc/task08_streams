package task1;

public class Task1 {
    public static void main(String... args) {
        MyInterface max = (a, b, c) -> { return MyInterface.max(a, b, c); };
        MyInterface maxWithoutFunc = (a, b, c) -> { return Math.max(Math.max(a, b), c); };

        MyInterface avar = (a, b, c) -> { return MyInterface.avarage(a, b, c); };
        MyInterface avarWithoutFunc = (a, b, c) -> { return (a + b + c) / 3; };

        System.out.println(max.calc(5, 4, 6));
        System.out.println(maxWithoutFunc.calc(5, 4, 6));

        System.out.println(avar.calc(5, 4, 6));
        System.out.println(avarWithoutFunc.calc(5, 4, 6));
    }
}

