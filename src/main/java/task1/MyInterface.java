package task1;

public interface MyInterface {
    public int calc(int a, int b, int c);
    static int max(int a, int b, int c) {
        return Math.max(Math.max(a, b), c);
    }
    static int avarage(int a, int b, int c) {
        return (a + b + c) / 3;
    }
}
