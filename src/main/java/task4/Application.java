package task4;

import java.io.IOException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Application {
    private static Logger log = LogManager.getLogger();
    public static void main(String[] args) {
        Controller c = new Controller(new Model());
        try {
            c.run();
        } catch(IOException ioe) {
            log.error("IOException while scanning users input!");
            throw new RuntimeException("IOException while scanning users input!");
        }

    }
}
