package task4;

import org.w3c.dom.ls.LSOutput;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Model {
    private int numberOfUnique;
    private List<String> ListOfUniques;
    private Map<String, Long> occurOfWords;
    private Map<Character, Long> occurOfChars;

    public void init(ArrayList<String> args) {
        ListOfUniques = listOfUniqueWords(args);
        numberOfUnique = ListOfUniques.size();
        occurOfWords = wordCount(args);
        occurOfChars = eachButUpperCase(args);
        display();
    }
    private void display() {
        System.out.println("numberOfUnique: " + numberOfUnique);
        System.out.println("ListOfUniques: " + ListOfUniques);
        System.out.println("occurOfWords: " + occurOfWords);
        System.out.println("occurOfChars: " + occurOfChars);

    }
    private List<String> listOfUniqueWords(ArrayList<String> args) {
        return args
                .stream().flatMap(s -> Stream.of(s.split(" ")))
                .peek((s)->{})
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    private static Map<String, Long> wordCount(ArrayList<String> args) {
        Stream<String> in = args.stream();
        List<String> list = in.flatMap(s -> Stream.of(s.split(" ")))
                .peek(s -> {})
                .collect(Collectors.toList());
        Stream<String> in2 = list.stream();
        Map<String, Long> result =
                in2.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return result;
    }

    private static Map<Character, Long> eachButUpperCase(ArrayList<String> args) {
        Stream<String> in = args.stream();
        List<String> list = in.flatMap(s -> Stream.of(s.split(" ")))
                .peek(s -> {})
                .collect(Collectors.toList());
        Map<Character, Long> charList = list.stream()
                .flatMapToInt(s -> s.chars())
                .mapToObj(c -> (char) c)
                .peek(c -> {})
                .filter(c -> !Character.isUpperCase(c))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return charList;
    }
}
