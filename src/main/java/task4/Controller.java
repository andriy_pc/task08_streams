package task4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Controller {
    private Model model;
    private Scanner in;
    public Controller(Model m) {
     model = m;
     in = new Scanner(System.in);
    }
    public void run() throws IOException {
        ArrayList<String> args = new ArrayList<>();
        String line;
        View.view("Insert String lines.\n" +
                "To stop insertion: insert empty line");
        line = in.nextLine();
        while(!line.equals("")) {
            args.add(line);
            line = in.nextLine();
        }
        model.init(args);
    }
}
