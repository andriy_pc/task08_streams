package taks2;

public class StringToCharArray  implements Command {
    public void execute(String s) {
        for(char c : s.toCharArray()) {
            System.out.print(c + " ");
        }
    }
}
