package taks2;

public class Model {
    private Command commandLambda = s -> {
        System.out.println(s.toUpperCase());
    };
    private Command commandRef = System.out::println;

    private Command commandObject = new StringToCharArray();
    //It's not truly anonymous...
    Command anonymousClass = new Command() {
        @Override
        public void execute(String s) {
            View.view("Anonymous...sh");
        }
    };

    public void invokeLambla(String s) {
        commandLambda.execute(s);
    }
    public void invokeRef(String s) {
        commandRef.execute(s);
    }
    public void invokeObject(String s) {
        commandObject.execute(s);
    }
    public void invokeAnon(String s) {
        anonymousClass.execute(s);
    }
}
