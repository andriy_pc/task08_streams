package taks2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger log = LogManager.getLogger();

    public static void main(String[] args) {
        Controller c = new Controller(new Model());
        try {
            c.run();
        } catch (IOException ioe) {
            log.error("IOException!!!");
            throw new RuntimeException("Application terminated" +
                    " because of IOException");
        }

    }
}
