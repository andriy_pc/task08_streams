package taks2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Scanner;

public class Controller {
    Controller(Model m) {
        model = m;
    }
    private static Logger log = LogManager.getLogger();

    private Model model;
    private Scanner in = new Scanner(System.in);
    private String mainMenu = "\nq quit;\n"
            +
            "lambda args\n"
            +
            "function args\n"
            +
            "anonymous args\n"
            +
            "object args";

    public void run() throws IOException {
        String command;
        String argument;
        String[] data;
        View.view(mainMenu);
        View.view("Please INSERT command:");
        String choice = in.nextLine();
      while(!choice.equals("q")) {
          data = choice.split(" ");
          command = data[0];
          argument = data[1];
          switch (command) {
              case "lambda" : model.invokeLambla(argument); break;
              case "function" : model.invokeRef(argument); break;
              case "anonymous" : model.invokeAnon(argument); break;
              case "object" : model.invokeObject(argument); break;
              default : {
                  View.view("Wrong input!");
                  log.error("Wrong input");
                  throw new RuntimeException("Wrong input");
              }
          }
          View.view(mainMenu);
          View.view("\nPlease INSERT command:");
          choice = in.nextLine();
      }
    }
}
